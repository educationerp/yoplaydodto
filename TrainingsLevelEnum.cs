﻿namespace YoPlayDoDto
{
    public enum TrainingsLevelEnum
    {
        Beginner = 1,
        Medium = 2,
        High = 3
    }
}