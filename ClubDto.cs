﻿using System;
using System.Collections.Generic;

namespace YoPlayDoDto
{
    public class ClubDto : BaseYoPlayDoDto
    {
        public string Name { get; set; }
        public string Address { get; set; }
        public double? Lat { get; set; }
        public double? Lon { get; set; }
        public string Country { get; set; }
        public string City { get; set; }
        public string TimeZone { get; set; }
    }
}
