﻿using System;

namespace YoPlayDoDto
{
    public class TeamDto : BaseYoPlayDoDto
    {
        public int ClubId { get; set; }
        public string Name { get; set; }
        public TrainingsLevelEnum TrainingsLevel { get; set; }
        public bool IsClosed { get; set; }
        public DateTime? CloseDateTimeUtc { get; set; }
    }
}