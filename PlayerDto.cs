﻿using System;
using System.Collections.Generic;

namespace YoPlayDoDto
{
    public class PlayerDto : BaseYoPlayDoDto
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime? BirthDate { get; set; }
        public int ParentId { get; set; }
        public ICollection<HistoryRecordDto> TeamsHistory { get; set; }
        public DateTime? FirstVisitDateTimeUtc { get; set; }
        public DateTime? RegisterDateTime { get; set; }

        /// <summary>
        /// Дата последнего изменения персональных данных ученика
        /// </summary>
        public DateTime? ChangesDate { get; set; }

        /// <summary>
        /// Подтверждены ли данные
        /// </summary>
        public bool IsConfirmed { get; set; }

        public DateTime? ConfirmationDate { get; set; }
    }
}