﻿using System;

namespace YoPlayDoDto
{
    public class LessonDto : BaseYoPlayDoDto
    {
        public int GroupId { get; set; }
        public DateTime DateTimeUtc { get; set; }
        public bool IsRemoved { get; set; }
    }
}