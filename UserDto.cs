﻿using System;
using System.Collections.Generic;

namespace YoPlayDoDto
{
    public class UserDto : BaseYoPlayDoDto
    {
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public DateTime? BirthDate { get; set; }
        public bool IsFederationManager { get; set; }
        public bool IsClubManager { get; set; }
        public bool IsTrainer { get; set; }
        public bool IsClient { get; set; }
        public bool IsSystemAdministrator { get; set; }
        public string City { get; set; }
        public string TimeZone { get; set; }
        public ICollection<int> ManagesClubId { get; set; }
        public ICollection<HistoryRecordDto> TrainingHistory { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
    }
}