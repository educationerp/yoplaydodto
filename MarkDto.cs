﻿namespace YoPlayDoDto
{
    public class MarkDto : BaseYoPlayDoDto
    {
        public int ChildId { get; set; }
        public int LessonId { get; set; }
        public bool IsRemoved { get; set; }
    }
}