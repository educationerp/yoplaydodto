﻿using System;

namespace YoPlayDoDto
{
    /// <summary>
    /// Запись о том, что тренер/игрок был добавлен/удалён из команды.
    /// Все даты в UTC
    /// </summary>
    public class HistoryRecordDto : BaseYoPlayDoDto
    {
        public DateTime Added { get; set; }
        public DateTime? Removed { get; set; }
        public int GroupId { get; set; }
    }
}